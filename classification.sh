#!/usr/bin/env sh
set -e

./caffe/build/examples/cpp_classification/classification.bin caffe/models/bvlc_reference_caffenet/deploy.prototxt caffe/models/bvlc_reference_caffenet/caffenet_train_image_iter_2400.caffemodel caffe/examples/Train_Image/generated_mean_train_lmdb.binaryproto caffe/examples/Train_Image/synset_words.txt /home/ubuntu/Desktop/repository_git/test_image.JPEG
#./build/tools/caffe train \
#    --solver=models/bvlc_reference_caffenet/solver.prototxt $@
