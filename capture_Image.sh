#!/usr/bin/env sh
set -e

./opencv_src/samples/gpu/cascadeclassifier --cascade opencv_src/data/haarcascades_cuda/haarcascade_frontalface_default.xml --camera 1

#./build/tools/caffe train \
#    --solver=models/bvlc_reference_caffenet/solver.prototxt $@
