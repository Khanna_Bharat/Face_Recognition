#!/usr/bin/env sh
# Compute the mean image from the imagenet training lmdb
# N.B. this is available in data/ilsvrc12

EXAMPLE=examples/Train_Image
DATA=examples/Train_Image
TOOLS=build/tools

$TOOLS/compute_image_mean $EXAMPLE/genrated_train_lmdb \
  $DATA/generated_mean_train_lmdb.binaryproto

echo "Done."
