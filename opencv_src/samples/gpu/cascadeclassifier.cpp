// WARNING: this sample is under construction! Use it on your own risk.
#if defined _MSC_VER && _MSC_VER >= 1400
#pragma warning(disable : 4100)
#endif


#include <iostream>
#include <iomanip>
#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/cudaobjdetect.hpp"
#include "opencv2/cudaimgproc.hpp"
#include "opencv2/cudawarping.hpp"

#include "tick_meter.hpp"

using namespace std;
using namespace cv;
using namespace cv::cuda;


static void help()
{
    cout << "Usage: ./cascadeclassifier_gpu \n\t--cascade <cascade_file>\n\t(<image>|--video <video>|--camera <camera_id>)\n"
            "Using OpenCV version " << CV_VERSION << endl << endl;
}


static void convertAndResize(const Mat& src, Mat& gray, Mat& resized, double scale)
{
    if (src.channels() == 3)
    {
        cv::cvtColor( src, gray, COLOR_BGR2GRAY );
    }
    else
    {
        gray = src;
    }

    Size sz(cvRound(gray.cols * scale), cvRound(gray.rows * scale));

    if (scale != 1)
    {
        cv::resize(gray, resized, sz);
    }
    else
    {
        resized = gray;
    }
}

static void convertAndResize(const GpuMat& src, GpuMat& gray, GpuMat& resized, double scale)
{
    if (src.channels() == 3)
    {
        cv::cuda::cvtColor( src, gray, COLOR_BGR2GRAY );
    }
    else
    {
        gray = src;
    }

    Size sz(cvRound(gray.cols * scale), cvRound(gray.rows * scale));

    if (scale != 1)
    {
        cv::cuda::resize(gray, resized, sz);
    }
    else
    {
        resized = gray;
    }
}


static void matPrint(Mat &img, int lineOffsY, Scalar fontColor, const string &ss)
{
    int fontFace = FONT_HERSHEY_DUPLEX;
    double fontScale = 0.8;
    int fontThickness = 2;
    Size fontSize = cv::getTextSize("T[]", fontFace, fontScale, fontThickness, 0);

    Point org;
    org.x = 1;
    org.y = 3 * fontSize.height * (lineOffsY + 1) / 2;
    putText(img, ss, org, fontFace, fontScale, Scalar(0,0,0), 5*fontThickness/2, 16);
    putText(img, ss, org, fontFace, fontScale, fontColor, fontThickness, 16);
}


static void displayState(Mat &canvas, bool bHelp, bool bGpu, bool bLargestFace, bool bFilter, double fps)
{
    Scalar fontColorRed = Scalar(255,0,0);
    Scalar fontColorNV  = Scalar(118,185,0);

    ostringstream ss;
    ss << "FPS = " << setprecision(1) << fixed << fps;
    matPrint(canvas, 0, fontColorRed, ss.str());
    ss.str("");
    ss << "[" << canvas.cols << "x" << canvas.rows << "], " <<
        (bGpu ? "GPU, " : "CPU, ") <<
        (bLargestFace ? "OneFace, " : "MultiFace, ") <<
        (bFilter ? "Filter:ON" : "Filter:OFF");
    matPrint(canvas, 1, fontColorRed, ss.str());

    // by Anatoly. MacOS fix. ostringstream(const string&) is a private
    // matPrint(canvas, 2, fontColorNV, ostringstream("Space - switch GPU / CPU"));
    if (bHelp)
    {
        matPrint(canvas, 2, fontColorNV, "Space - switch GPU / CPU");
        matPrint(canvas, 3, fontColorNV, "M - switch OneFace / MultiFace");
        matPrint(canvas, 4, fontColorNV, "F - toggle rectangles Filter");
        matPrint(canvas, 5, fontColorNV, "H - toggle hotkeys help");
        matPrint(canvas, 6, fontColorNV, "1/Q - increase/decrease scale");
    }
    else
    {
        matPrint(canvas, 2, fontColorNV, "H - toggle hotkeys help");
    }
}


int main(int argc, const char *argv[])
{
int adjust_size = 0;
#if 1 
    if (argc == 1)
    {
        help();
        return -1;
    }
#endif
//getCudaEnabledDeviceCount
//Use this function before any other GPU functions calls. If OpenCV is compiled without GPU 
//support, this function returns 0.
    if (getCudaEnabledDeviceCount() == 0)
    {
        return cerr << "No GPU found or the library is compiled without CUDA support" << endl, -1;
    }

//getDevice
//Returns the current device index set by cuda::setDevice or initialized by default.
//printShortCudaDeviceInfo will print
//Device 0:  "NVIDIA Tegra X1"  3994Mb, sm_53, Driver/Runtime ver.8.0/7.0
    cv::cuda::printShortCudaDeviceInfo(cv::cuda::getDevice());
    string cascadeName;
    string inputName;
    bool isInputImage = false;
    bool isInputVideo = false;
    bool isInputCamera = false;

    for (int i = 1; i < argc; ++i)
    {
        if (string(argv[i]) == "--cascade")
{
            cascadeName = argv[++i];
//argv[i]= ../../data/haarcascades_cuda/haarcascade_frontalface_default.xml
}
#if 0
        else if (string(argv[i]) == "--video")
        {
// not coming in this loop
            inputName = argv[++i];
            isInputVideo = true;
        }
#endif
        else if (string(argv[i]) == "--camera")
        {
            inputName = argv[++i];
            isInputCamera = true;
        }
        else if (string(argv[i]) == "--help")
        {
            help();
            return -1;
        }
#if 0
        else if (!isInputImage)
        {
            inputName = argv[i];
            isInputImage = true;
        }
#endif
        else
        {
            cout << "Unknown key: " << argv[i] << endl;
            return -1;
        }
    }
//create
//Loads the classifier from a file. Cascade type is detected automatically by constructor parameter.
//cascadeName
//Name of the file from which the classifier is loaded. Only the old haar classifier (trained by the haar training 
//application) and NVIDIA's nvbin are supported for HAAR and only new type of OpenCV XML cascade supported for LBP.
//The working haar models can be found at opencv_folder/data/haarcascades_cuda/
    Ptr<cuda::CascadeClassifier> cascade_gpu = cuda::CascadeClassifier::create(cascadeName);

//load to load a .xml classifier file. It can be either a Haar or a LBP classifer
    cv::CascadeClassifier cascade_cpu;
    if (!cascade_cpu.load(cascadeName))
    {
        return cerr << "ERROR: Could not load cascade classifier \"" << cascadeName << "\"" << endl, help(), -1;
    }
#if 0
    //VideoCapture capture;
    VideoCapture capture("nvcamerasrc ! video/x-raw(memory:NVMM), width=(int)1280,height=(int)720,format=(string)I420, framerate=(fraction)24/1 ! nvvidconv flip-method=2 ! video/x-raw, format=(string)BGRx ! videoconvert ! video/x-raw, format=(string)BGR ! appsink"); //open the default camera
#endif


//VideoCapture
//Class for video capturing from video files, image sequences or cameras. The class provides C++ API 
//for capturing video from cameras or for reading video files and image sequences. 
    VideoCapture capture("nvcamerasrc ! video/x-raw(memory:NVMM), width=(int)720,height=(int)480,format=(string)I420, framerate=(fraction)24/1 ! nvvidconv flip-method=2 ! video/x-raw, format=(string)BGRx ! videoconvert ! video/x-raw, format=(string)BGR ! appsink"); //open the default camera

//The class Mat represents an n-dimensional dense numerical single-channel or multi-channel array. It can be used to store real or complex-valued vectors and matrices, grayscale or color 
// images, voxel volumes, vector fields, point clouds, tensors, histograms (though, very high-dimensional histograms may be better stored in a SparseMat ).
    Mat image;
#if 0
    if (isInputImage)
    {
        image = imread(inputName);
        CV_Assert(!image.empty());
    }
else if (isInputVideo)

    {
        capture.open(inputName);
        CV_Assert(capture.isOpened());
    }
else
#endif
        //capture.open(atoi(inputName.c_str()));
//isOpened
//// check if we succeeded in opening camera
        CV_Assert(capture.isOpened());

//namedWindow
//Creates a window.
//
    namedWindow("result", 1);

    Mat frame, frame_cpu, gray_cpu, resized_cpu, frameDisp,croppedFaceImage;
	Mat save_img;
// put rectangle on face on decting the face
    vector<Rect> faces;
    GpuMat frame_gpu, gray_gpu, resized_gpu, facesBuf_gpu;


    /* parameters */
    bool useGPU = true;
    double scaleFactor = 1.0;
    bool findLargestObject = false;
    bool filterRects = true;
    bool helpScreen = false;

    //for (;;)
while(1)
    {
        if (isInputCamera || isInputVideo)
        {
//frame was initialized to zero then it is storede with capture valuesget a new frame from camera
            capture >> frame;
	// imwrite("andrewsong.jpg",frame);
            if (frame.empty())
            {
                break;
            }
        }
//Copies the matrix to another one.
	//final matrix in frame_cpu
        //(image.empty() ? frame : image).copyTo(frame_cpu);
        
	if ( image.empty() )
        	frame.copyTo(frame_cpu);
	//pefroms upload data to GpuMat (Non-Blocking call)
	//frame_gpu.upload(image.empty() ? frame : image);
        if ( image.empty() )
		frame_gpu.upload(frame);


//convertAndResize(const Mat& src, Mat& gray, Mat& resized, double scale)
        convertAndResize(frame_gpu, gray_gpu, resized_gpu, scaleFactor);
        convertAndResize(frame_cpu, gray_cpu, resized_cpu, scaleFactor);
        TickMeter tm;
        tm.start();

        if (useGPU)
        {
            cascade_gpu->setFindLargestObject(findLargestObject);
            cascade_gpu->setScaleFactor(1.2);
            cascade_gpu->setMinNeighbors((filterRects || findLargestObject) ? 4 : 0);

            cascade_gpu->detectMultiScale(resized_gpu, facesBuf_gpu);
            cascade_gpu->convert(facesBuf_gpu, faces);
		//imwrite("test.jpg",frame);
        }
        else
        {
            Size minSize = cascade_gpu->getClassifierSize();
            cascade_cpu.detectMultiScale(resized_cpu, faces, 1.2,
                                         (filterRects || findLargestObject) ? 4 : 0,
                                         (findLargestObject ? CASCADE_FIND_BIGGEST_OBJECT : 0)
                                            | CASCADE_SCALE_IMAGE,
                                         minSize);
		
        }
// put rectangle on the face
        for (size_t i = 0; i < faces.size(); ++i)
        {
            rectangle(resized_cpu, faces[i], Scalar(255));
//resized_cpu is the image with rectangle on the face
		//imwrite("gray_scale_W_Rec.jpg",resized_cpu);
        }
//imwrite("gray_scale_W_Rec.jpg",resized_cpu);
        tm.stop();
        double detectionTime = tm.getTimeMilli();
        double fps = 1000 / detectionTime;

        //print detections to console
//faces.size() number of faces detected
	
        cout << setfill(' ') << setprecision(2);
        cout << setw(6) << fixed << fps << " FPS, " << faces.size() << " det";
        if ((filterRects || findLargestObject) && !faces.empty())
        {
            for (size_t i = 0; i < faces.size(); ++i)
            {
                cout << ", [" << setw(4) << faces[i].x
                     << ", " << setw(4) << faces[i].y
                     << ", " << setw(4) << faces[i].width
                     << ", " << setw(4) << faces[i].height << "]";
		//frame(cv::Rect(faces[i].x, faces[i].y, faces[i].width, faces[i].height)).copyTo(croppedFaceImage);
		adjust_size = 256 - faces[i].width;
cout<<"adjust_size " <<adjust_size <<endl;
		frame(cv::Rect((faces[i].x), (faces[i].y), (faces[i].width)+adjust_size, (faces[i].height)+adjust_size)).copyTo(croppedFaceImage);
            }
		//imwrite("Colored_Image.jpg",frame);
        }
        cout << endl;
		imwrite("test_image.JPEG",croppedFaceImage);
//Converts an image from one color space to another.
//cv::cvtColor (InputArray src, OutputArray dst, int code, int dstCn=0)
//Note that the default color format in OpenCV is often referred to as RGB but it is actually BGR (the bytes are reversed).
// So the first byte in a standard (24-bit) color image will be an 8-bit Blue component,
        cv::cvtColor(resized_cpu, frameDisp, COLOR_GRAY2BGR);
//static void displayState(Mat &canvas, bool bHelp, bool bGpu, bool bLargestFace, bool bFilter, double fps)
        displayState(frameDisp, helpScreen, useGPU, findLargestObject, filterRects, fps);
        imshow("result", frameDisp);
//wait for an event for a delay of 5
        char key = (char)waitKey(5);
        if (key == 27)
        {
            break;
        }

        switch (key)
        {
        case ' ':
            useGPU = !useGPU;
            break;
        case 'm':		
        case 'M':
            findLargestObject = !findLargestObject;
            break;
        case 'f':
        case 'F':
            filterRects = !filterRects;
            break;
        case '1':
            scaleFactor *= 1.05;
            break;
        case 'q':
        case 'Q':
            scaleFactor /= 1.05;
            break;
        case 'h':
        case 'H':
            helpScreen = !helpScreen;
            break;
        }

    }

    return 0;
}
