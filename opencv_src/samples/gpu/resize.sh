array=()
find . -iname \*.JPEG -print0 >tmpfile
while IFS=  read -r -d $'\0'; do
    array+=("$REPLY")
done <tmpfile
rm -f tmpfile

echo ${array[*]}

for i in "${array[@]}"
do
	convert $i -resize 256x256! $i
done

