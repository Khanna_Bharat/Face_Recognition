This project implements face detection using OpenCV and recognition using Caffe, deep learning framework with our own database. 

For face detection the code written in C++ language is referring to cascadeclassifier Project in openCV. 

For face recognition this project uses caffenet model. 

On detecting the face email is sent to the desired person with the name of detected person in attached file.

Parameter dependencies:

train_val.prototxt --> num_output: Number of class

train_val.prototxt --> genrated_mean_train_lmdb ---> batch_size: Number of Images in the train folder

train_val.prototxt --> transform_param ---> crop_size: 256

Note:

Total Number of train Images = batch_size in train X max_iteration

Total Number of val Images = batch_size in val X test_iteration

where,

max_iteration >= test_iteration X batch_size in val

change path in step 12 and 13 as per your system.

1) cd Desktop

2) mkdir repository_git

3) git clone git@gitlab.com:Khanna_Bharat/Face_Recognition.git

4) extract all the folders inside repository_git folder.

5) cd caffe

6) make all -j8

7) make test -j8

8) ./examples/Train_Image/train_image.sh

9) ./examples/Train_Image/create_train_image.sh

10) ./examples/Train_Image/make_train_image_mean.sh

11) cd ..

12) ./capture_Image.sh

/************************************************************************************************************/

sudo apt-get install ssmtp

sudo apt-get install mailutils

sudo gedit /etc/ssmtp/ssmtp.conf

/*****************************change here ***********************/

root=yourEmail@address

mailhub=smtp.gmail.com:587

rewriteDomain=gmail.com

hostname=localhost

UseTLS=Yes

UseSTARTTLS=Yes

AuthUser=yourEmail@address

AuthPass=youremailAccountPassword

TLS_CA_File=/etc/pki/tls/certs/ca-bundle.crt

AuthMethod=LOGIN

FromLineOverride=yes

/*****************************change End ***********************/

For testing Email.

ssmtp yourEmailaddress

uuencode file_on_your_system name_the_file_to reflect_in_attachment | mail email_address_where_you_want_to_send

/****************************************************************************************/

13) ./regex

